#
#   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
#  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
#  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
#  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
#
#  Created, owned and maintained by Nonatomic Ltd.
#  https://www.nonatomic.co.uk
#  support@nonatomic.co.uk
#
#  Copyright 2016 - All Rights Reserved
#

# Bitbucket CLI create a repo

# (1) copy the `bb` file into ~/usr/local/bin
# (2) from command line run `sudo chown $USER /usr/local/bin/bb`
# (3) now you can access `bb create` and pass params
# (4) for list of params `-h | --help`

CYAN='\033[0;36m'
PURPLE='\033[0;35m'
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

PRIVATE=true
NAME=''
USER=''
PW=''
OWNER=''
WEBSITE=''
# DESCRIPTION=''
LANGUAGE=''
HAS_WIKI=true
HAS_ISSUES=true
CREATOR=''
NO_PUBLIC_FORKS=true
DEBUG=false

function create {

	while [[ $# > 1 ]]
	do
	key="$2"

	case $key in
	    -r|--private)
	    PRIVATE="$3"
	    shift
	    ;;
	    -n|--name)
	    NAME="$3"
	    shift
	    ;;
	    -u|--user|--username)
	    USER="$3"
	    shift
	    ;;
	    -p|--password)
	    PW="$3"
	    shift
	    ;;
	    -w|--website)
	    WEBSITE="$3"
	    shift
	    ;;
	    # -d|--desc|--description)
	    # DESCRIPTION="$3"
	    # shift
	    # ;;
	    -l|--lang|--language)
	    LANGUAGE="$3"
	    shift
	    ;;
	    -wiki)
	    HAS_WIKI="$3"
	    shift
	    ;;
	    -i|-issues)
	    HAS_ISSUES="$3"
	    shift
	    ;;
	    -c|-creator)
	    CREATOR="$3"
	    shift
	    ;;
	    --debug)
	    DEBUG=true
	    shift
	    ;;
	    -f|-no-forks)
	    NO_PUBLIC_FORKS="$3"
	    shift
	    ;;
	    -h|-help)
	    usage
	    return
	    shift
	    ;;
	    -o|--owner)
	    OWNER="$3"
	    shift
	    ;;
	    *)
	    	# unknown option
	    ;;
	esac
	shift # past argument or value
	done

	#check required params are set
	if [[ -z "$USER" ]]
	then
		echo ""
		echo -n -e "${YELLOW}Enter your username: ${NC}"
		read USER
	fi

	if [[ -z "$PW" ]]
	then
		echo ""
		echo -n -e "${YELLOW}Enter your password: ${NC}"
		read -s PW
	fi

	if [[ -z "$NAME" ]]
	then
		echo ""
		echo -n -e "${YELLOW}Enter your repo name: ${NC}"
		read NAME
	fi	

	if [[ -z "$OWNER" ]]
	then
		echo ""
		echo -n -e "${YELLOW}Enter your repos owner: ${NC}"
		read OWNER
	fi

	echo -e "${PURPLE}Creating repo..."
	RESULT=$(curl --user $USER:$PW https://api.bitbucket.org/1.0/repositories/ -d name=$NAME -d is_private=$PRIVATE -d owner=$OWNER -d has_wiki=$HAS_WIKI -d description="$DESCRIPTION" -d has_issues=$HAS_ISSUES -d language=$LANGUAGE -d creator=$CREATOR -d no_public_forks=$NO_PUBLIC_FORKS)

	if [ -z "$RESULT" ] || [ "$RESULT" == "Bad Request" ]
	then
		echo -e "${RED}Failed${NC}"
	else
		echo -e "${GREEN}Success${NC}"
	fi

	if [ $DEBUG ]
	then
		echo -e "${PURPLE}$RESULT${NC}"
		usage
	fi
}

function usage {
	echo -e "${CYAN}To use the bitbucket cli...."
	echo ""
	echo -e "${GREEN}Methods"
	echo -e "${PURPLE}   create - used to create a remote remo"
	echo ""
	echo -e "${GREEN}Params"
	echo -e "${PURPLE}   -r|--private ${CYAN}$PRIVATE"
	echo -e "${PURPLE}   -n|--name ${CYAN}$NAME"
	echo -e "${PURPLE}   --u|--user|--username ${CYAN}$USER"
	echo -e "${PURPLE}   -p|--password"
	echo -e "${PURPLE}   -w|--website ${CYAN}$WEBSITE"
	# echo -e "${PURPLE}   -d|--desc|--description ${CYAN}$DESCRIPTION"
	echo -e "${PURPLE}   -l|--lang|--language"
	echo -e "${PURPLE}   -wiki ${CYAN}$HAS_WIKI"
	echo -e "${PURPLE}   -i|-issues ${CYAN}$HAS_ISSUES"
	echo -e "${PURPLE}   -c|-creator ${CYAN}$CREATOR"
	echo -e "${PURPLE}   -f|-no-forks ${CYAN}$NO_PUBLIC_FORKS"
	echo -e "${PURPLE}   -h|-help ${CYAN}$HELP"
	echo -e "${PURPLE}   -o|--owner ${CYAN}$OWNER${NC}"
	echo -e "${PURPLE}   --debug ${CYAN}$DEBUG${NC}"
}


while [[ $# > 0 ]]
	do
	key="$1"

	case $key in
	    create)
	    create $@
	    shift
	    ;;
	    -h|--help)
	    usage
	    shift
	    ;;
	esac
	shift
	done